from django.urls import path
from .views import path_list, post_detail, post_new,post_edit

urlpatterns = [
    path('', path_list, name="path_list"),
    path('post/<int:id>/', post_detail, name="post_detail"),
    path('post/new/', post_new, name="post_new"),
    path('post/<int:id>/edit/', post_edit, name="post_edit"),
]